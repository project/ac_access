
/***************************************************************************************************/
/*                                                                                                 */
/*                             Bottom Up Access/Audience                                           */
/*                                                                                                 */
/* By Mixel Kiemen 12-2006, a remake of the earlier implemented javascript by Wouter Van den Bosch */
/* Created and used for the KNOSOS project                                                         */
/*                                                                                                 */
/***************************************************************************************************/
$(document).ready(ac_access_init);

//startup: make the tabs, the jqselects and do the updates.
function ac_access_init()
{
  var tabs = document.getElementById('jqs_tabs').getElementsByTagName('A');
  for (var i = 1; i < tabs.length; i++)
  { 
    jqselect_init(tabs[i].innerHTML);
  }

//make sure all selections exist even if not editable
  ac_access_init_selections('user'); 
  ac_access_init_selections('group'); 
  ac_access_init_selections('role'); 

  ac_access_tab_init(tabs);
  ac_access_submit_init(tabs);

  var added = false;
  for (var i = 1; i < tabs.length; i++) 
    if(ac_access_advanced_init_level(tabs[i].innerHTML)) //did we add somthing?
      added = true;

  ac_access_profile_init(added);
  ac_access_list_update();

}

function  ac_access_init_selections(level)
{

  var selections = jqselect_find(level,'selections');
  if(selections != undefined){
   return; //its ok, the selections list exists.
   }
   
  selections = document.createElement('select');
  $(selections).attr('id', level+'_jqs_selections');
  $(selections).attr('class', 'invisible');
   var extrafield=document.getElementById('jqs_tabs');
  $(extrafield).append(selections);
 
} 
//jqselect hook for user audience buttons, handle updates
function ac_access_jqselectAPI(type, op, id, value, tag)
{	
 
 switch (op)
  {
    case 'after_remove':
    case 'after_add' :
      ac_access_list_update();
      ac_access_submit_update();
      
    break;
    case 'add' :
    case 'remove' :
      ac_access_advanced_update(type, op, id, value, tag);
    break;
  }
}

/********************************************************/
/*                        profile                       */
/********************************************************/
function ac_access_profile_init(added)
{
  $('#edit-access-profile').change(ac_access_profile_set_init);

  $("#ac_access_save_profile_button").click(ac_access_profile_save);

  if(added) //we don't have an empty page, values are already set.
   return;

  var select_list = document.getElementById('selected_jqs_list');
  var options = document.getElementById('edit-access-profile');
  var opt = $("option[@selected]", options);
  var name = opt.text();
  var realm = opt.parent().attr('label');

  ac_access_profile_set(realm+'-'+name);
}

//this is a terrible function to get the optgroup from the selectedIndex => you can't go to parrent like in the init. 
function ac_access_profile_set_init()
{

  var opt = this.options[this.selectedIndex];
  var name = $(opt).text();
  var max = $(this).children().size();
  var iter = 0;
  var last = 0;
  var realm = '';

  realm = $(this.childNodes[0]).attr('label');

 
  while(iter < max && last <  this.selectedIndex)
  {
    last += $(this.childNodes[iter]).children().size() -1;
    realm = $(this.childNodes[iter]).attr('label');
    iter++;
  }  
  
  
  ac_access_profile_set(realm+'-'+name);
  
}

function ac_access_profile_set(value)
{
  ac_access_profile_set_level(value);
  ac_access_profile_clear();
  ac_access_profile_set_audiences(value, 'user');
  ac_access_profile_set_audiences(value, 'group');
  ac_access_profile_set_audiences(value, 'role');

  ac_access_list_update();
}

function ac_access_profile_set_level(value)
{
  var inputs = document.getElementById('access_general').getElementsByTagName('input');
  var accessLevel = document.getElementById('edit-ac-access-'+value+'-level').getAttribute('value');
  for (var i = 0; i < inputs.length; i++){ 
    if (accessLevel == inputs[i].getAttribute('value')){
      $(inputs[i]).attr("checked","checked");
      }else{
      $(inputs[i]).removeAttr("checked");
      }
   }   
}

function ac_access_profile_set_audiences(value, level)
{
  var selections = jqselect_find(level,'selections');

  var suggestions = jqselect_find(level,'suggestions');
  var audience = document.getElementById('edit-ac-access-'+value+'-'+level);
  var grants = document.getElementById('edit-ac-access-'+value+'-'+level+'-grant');

 //we may be checking an undefined audiences, in this case just skipp.
 if(audience == undefined || grants == undefined) 
   return;

  var text = level+' '+grants.length+' => ';
  for (var j = 0; j < audience.length; j++)
  {
    // a mix between jqselect_add_suggestions and jqselect_add_free_select, making sure the suggestion list is not changed
    var opt = audience.options[j];

    var thisindex = -1;

    if(suggestions != undefined)
      thisindex = jqselect_free_is_suggestion(suggestions, opt.text);

    if(thisindex != -1)
    {
      $(suggestions.options[thisindex]).remove();
      $(opt).attr('class','suggestion'); //add this class for the remove => jqselect_remove_or_replace
    }
    $(selections).append($(opt).clone());
    var grant = 0;

    text += grants.options[j]+' ';

    if(grants.options[j])
      grant = grants.options[j].text;

    jqselect_callapi(level, 'add', $(opt).val(), opt.text, grant);
  }
  jqselect_callapi(level, 'after_add', -1, '','');


}

function ac_access_profile_clear()
{
  ac_access_profile_clear_level('user');
  ac_access_profile_clear_level('group');
  ac_access_profile_clear_level('role');
}

function ac_access_profile_clear_level(level)
{
  var selections = jqselect_find(level,'selections');
  var suggestions = jqselect_find(level,'suggestions');

  //we need to check if it is a jqs or if there is a static selection list
  if(suggestions == undefined)
    $(selections).empty();
  else
    while(selections.length)
    {
      jqselect_callapi(level, 'remove', 0, selections.options[0].text ,'');
      jqselect_remove_or_replace($(selections.options[0]),suggestions);
    } 
}

function ac_access_profile_save()
{
  //first get the name of the profile from the input text

 
  var name1 = $("input[@type='text']",$(this).parent()).val();
  var name=name1.replace(/ /g,"-");
  
  if(name==""){
  	$('#selected_jqs_list').html('Profile name should not be empty');
  	return;
  }
  

  var level = $("input[@checked]", document.getElementById('access_general')).val();
  var grant = ac_access_profile2string('user');
  grant += ':'+ac_access_profile2string('group');
  grant += ':'+ac_access_profile2string('role');
  
  $.ajax(
  {
        type: "POST",
        url: $("input[@type='hidden']",$(this).parent()).val(),
        data: "ac_access_profile_name="+name+"&ac_access_profile_level="+level+"&ac_access_profile_audience="+grant,
        success: function (xmlAudience) { 
			ac_access_profile_update(name); 
	
	},
        error: function (xmlhttp) 
        {
          alert('profile save error: '+msg);
        }
  });
}

function ac_access_profile_update(name) 
{
  var slct = document.getElementById('edit-access-profile');
  var opts = $("optgroup[@label='personal']", slct).children();
  var opID = ac_access_profile_update_getID(opts, name);

  // if its new we need to add a name to the list, if its old we need to delete the hidden
  if(opID == -1)
    opID = ac_access_profile_update_list(name);
  else 
    ac_access_profile_update_hidden_remove(name);
  slct.selectedIndex = opID;
  ac_access_profile_update_hidden_add(name);

  ac_access_profile_set('personal-'+name);
}

function ac_access_profile_update_getID(opts, name) 
{
  for(var i = 0; i < $(opts).size(); i++)
    if($(opts[i]).text() == name)
      return i;  

  return -1;
}

function ac_access_profile_update_list(name)
{
  var opts = $("optgroup[@label='personal']", '#edit-access-profile');
  var op = document.createElement('option');
  $(op).attr('value', $(opts).children().length);
  op.innerHTML = name;
  $(opts).append(op);

  return $(op).val();
}
//we know the name refeers to an option in the "personal" list.
function ac_access_profile_update_hidden_remove(name)
{
  var idName = 'edit-ac-access-personal-'+name;
  $('#'+idName+'-level').remove();
  $('#'+idName+'-user').remove();
  $('#'+idName+'-user-grant').remove();
  $('#'+idName+'-group').remove();
  $('#'+idName+'-group-grant').remove();
  $('#'+idName+'-role').remove();
  $('#'+idName+'-role-grant').remove();
  $('#'+idName+'-levels').remove();
}

function ac_access_profile_update_hidden_add(name)
{


  var extrafield=document.getElementById('access_profile');

  var idName = 'edit-ac-access-personal-'+name;
  var level = document.createElement('input');
  var lName = $("input[@type=radio][@name='access_level'][@checked]").val();
  $(level).attr('type', 'hidden');
  $(level).attr('id', idName+'-level');
  $(level).attr('value', lName);
  $(extrafield).append(level);

  var users = $('#user_jqs_selections').clone();
  $(users).attr('id', idName+'-user');
  $(users).attr('class', 'invisible');
  $(extrafield).append(users);
  
  var uGrant = ac_access_grant_radio2select('user');
  $(uGrant).attr('id', idName+'-user-grant');
  $(uGrant).attr('class', 'invisible');
  $(extrafield).append(uGrant);

  var groups = $('#group_jqs_selections').clone();
  $(groups).attr('id', idName+'-group');
  $(groups).attr('class', 'invisible');
  $(extrafield).append(groups);
  
  var gGrant = ac_access_grant_radio2select('group');
  $(gGrant).attr('id', idName+'-group-grant');
  $(gGrant).attr('class', 'invisible');
  $(extrafield).append(gGrant);

  var roles = $('#role_jqs_selections').clone();
  $(roles).attr('id', idName+'-role');
  $(roles).attr('class', 'invisible');
  $(extrafield).append(roles);
  
  var rGrant = ac_access_grant_radio2select('role');
  $(rGrant).attr('id', idName+'-role-grant');
  $(rGrant).attr('class', 'invisible');
  $(extrafield).append(rGrant);
}

function ac_access_grant_radio2select(level)//, op, id, value, tag)
{	
  var radios = document.getElementById(level+'_advanced_settings').childNodes;
  var grants = document.createElement('select');
  var opt;

  for(var id = 0; id < radios.length; id++)
  {
    opt = document.createElement('option');
    $(opt).attr('value', id);
    opt.innerHTML = $('input[@checked]', radios[id]).val();
    $(grants).append(opt);
   }	
  return grants;
}

function ac_access_profile2string(level)
{
  var selections = jqselect_find(level,'selections');
  var a_div = document.getElementById(level+'_advanced_settings');
  var text= '';
  for(var i =0; i < selections.length; i++)
    text += selections[i].value + "_"+$("input[@checked]", a_div.childNodes[i]).val()+" ";
  return text;
}


/********************************************************/
/*                        tabs                          */
/********************************************************/

//create the three tabs general (where the access is defined) and the two jqselect lists for users and groups
function ac_access_tab_init(tabs) 
{	
  $(tabs[0]).click(function(){ac_access_load_tab(tabs, 0)}); 
  if( 1 < tabs.length)
    $(tabs[1]).click(function(){ac_access_load_tab(tabs, 1)});
  if( 2 < tabs.length)
    $(tabs[2]).click(function(){ac_access_load_tab(tabs, 2)});	
  if( 3 < tabs.length)
    $(tabs[3]).click(function(){ac_access_load_tab(tabs, 3)});	

  ac_access_load_tab(tabs, 0);
}

//the tab switch function, basicly shows and hides what is needed
//we know the current is == tab, when curren == 0 we have the access level (general tab)
function ac_access_load_tab(tabs, current)
{
  ac_access_view_tab(tabs, current);
	
  var access = $(document.getElementById('access_general'));

  var divs = new Array();
  for (var i = 1; i < tabs.length; i++) 
     divs[i] =  $(document.getElementById(tabs[i].innerHTML));

  if(current)
    access.addClass('invisible');
  else
    access.removeClass('invisible');

  for (var i = 1; i < tabs.length; i++) 
    if(i == current)
      divs[i].removeClass('invisible');
    else
      divs[i].addClass('invisible');
}

//a help function to switch the active-tab
function ac_access_view_tab(tabs, current)
{
  for (var i = 0; i < tabs.length; i++) 
    if(i == current)
      $(tabs[i]).addClass('tab_active');			
    else		
      $(tabs[i]).removeClass('tab_active');
}

/********************************************************/
/*                      submit                          */
/********************************************************/
function ac_access_submit_init(tabs) 
{		
  var inputs = document.getElementById('access_general').getElementsByTagName('input');
  var index = 0;
  var submitButton = ac_access_get_submit();
  
  if(submitButton != undefined)$(submitButton).click(function(){ac_access_submitting(tabs)});

// to bad a 'for' or a 'while' doesn't work, just like with the tabs, (somthing with creating lambda's??)
  if(inputs.length > 0)$(inputs[0]).click(function(){ac_access_submit_set(inputs[0].value)});
  if(inputs.length > 1)$(inputs[1]).click(function(){ac_access_submit_set(inputs[1].value)});
  if(inputs.length > 2)$(inputs[2]).click(function(){ac_access_submit_set(inputs[2].value)});
  if(inputs.length > 3)$(inputs[3]).click(function(){ac_access_submit_set(inputs[3].value)});
  if(inputs.length > 4)$(inputs[4]).click(function(){ac_access_submit_set(inputs[4].value)});	
}

function ac_access_submit_update() 
{

  var inputs = document.getElementById('access_general').getElementsByTagName('input');

  for (var i = 0; i < inputs.length; i++) 
    if (inputs[i].checked){
    
      	var accessLevel = inputs[i].getAttribute('value');
  		ac_access_submit_set(accessLevel);
  		}
  
}

function ac_access_submit_set(accessLevel) 
{  
  var submitButton = ac_access_get_submit();

  if(submitButton != undefined){
  switch(accessLevel) 
  {
    case 'user_access' :
      var users = jqselect_find('user','selections');
      submitButton.disabled = (users.options.length == 0);
    break;
    case 'group_access' :
      var groups = jqselect_find('group','selections');
      submitButton.disabled = (groups.options.length == 0);
    break;
    case 'role_access' :
      var roles = jqselect_find('role','selections');
      submitButton.disabled = (roles.options.length == 0);
    break;
    default:
      submitButton.disabled = true;
    break;
  }
  }
}

//get the submit button
function ac_access_get_submit() 
{	
 var opButtons = $("input:submit.form-submit[@value=Submit]");

	
  for (var i = 0; i < opButtons.length; i++)
    if (opButtons[i].value == "Submit") 
      return opButtons[i]; 
      
}

function ac_access_submitting(tabs) 
{

  for (var i = 1; i < tabs.length; i++) 
  {
    var list = jqselect_find(tabs[i], 'selections');
    
    for (var j = 0; j < list.length; j++) 
      list[j].selected = true;
  }
}

/********************************************************/
/*                       listing                        */
/********************************************************/
//get both jqselect list and show it as an (information) summary 
function ac_access_list_update() 
{
  var select_text = '<b><i>Selection: </i></b>';

  var list_text = ac_access_list_update_level('user');
  list_text += ac_access_list_update_level('group');
  list_text += ac_access_list_update_level('role');

  if (list_text == undefined)
    list_text = '<b><i>No selections.</b>';
  else
    list_text = '<b><i>Selection: </i></b>'+list_text;

  $('#selected_jqs_list').html(list_text);
}	

function ac_access_list_update_level(level) 
{
    var list = jqselect_find(level, 'selections');  

    if (list == undefined)
      return '';

    if (list.length)
      return '<br><b>'+level+'</b><i> '+ ac_access_list_print(list) + '</i>';

    return '';
}

//a simple function to add separation between the elements in the list
function ac_access_list_print(list) 
{
  var str ='';
  for(i=0; i< list.length; i++)
    str +=  ' | '+ $(list[i]).text();
  return str;
}

/********************************************************/
/*                      advanced                        */
/********************************************************/
//the first time the system loads we need to get all the elements already in the selection-list to the advanced setup
function ac_access_advanced_init_level(level)
{
	
  var select = jqselect_find(level, 'selections').options;
  var type = document.getElementById('edit-audience-types-'+level).options;
  var s_div = document.getElementById(level+'_advanced_settings');
  for (var i = 0; i < select.length; i++) 
    $(s_div).append(ac_access_create_radios($(type[i]).val(),level, select[i].text, type[i].text));
  $(s_div).removeClass('invisible');
  return select.length;
}
//when a item is added or deleted update the advanced setup
function ac_access_advanced_update(level, op, id, value, tag)
{	
  var at_div = document.getElementById(level+'_advanced_settings');
  if(op == 'add') 
    $(at_div).append(ac_access_create_radios(id, level, value, tag));
  else
    at_div.removeChild(at_div.childNodes[id]); 
}

//create the item as a set of a label three radios buttons and some dives for layout
function ac_access_create_radios(idName, typeName, valueName, checked)
{	
  var r_div = document.createElement('div');
  var r_label = document.createElement('div');
  var r_label_span = document.createElement('span');

  var r_view  = ac_access_create_radio(idName, typeName, 'view', 0, checked);
  var r_edit = ac_access_create_radio(idName, typeName, 'edit', 1, checked);
  var r_delete = ac_access_create_radio(idName, typeName, 'delete', 2, checked);
  

  r_label_span.innerHTML = valueName;
  $(r_div).addClass('advanced_div');
  $(r_label).addClass('advanced_div_label');

  $(r_label).append(r_label_span);
  $(r_div).append(r_label);

  $(r_div).append(r_view);
  $(r_div).append(r_edit);
  $(r_div).append(r_delete);

  return r_div;
}

//create on of the radio buttons
//notice the name of the radios this is important to get the value back
function ac_access_create_radio(idName, typeName, r_type, value, checked)
{

  var at_ratio = document.createElement('div');
  var check=(value == checked);
  
  
  //in IE one can not dynamically set the property name
  if(check){
  	$(at_ratio).html('<input type=radio name=edit[ac-access]['+typeName+']['+idName+'] value='+value+' checked></input>');
  	}else{
  	$(at_ratio).html('<input type=radio name=edit[ac-access]['+typeName+']['+idName+'] value='+value+'></input>');
  	
  	}
  
	
  var at_option = document.createElement('div');
  $(at_option).addClass('advanced_div_option');
  $(at_option).append(at_ratio);

  return at_option;
}
